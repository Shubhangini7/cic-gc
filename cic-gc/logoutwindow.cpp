#include "logoutwindow.h"
#include "ui_logoutwindow.h"

LogOutWindow::LogOutWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LogOutWindow)
{
    ui->setupUi(this);
}

LogOutWindow::~LogOutWindow()
{
    delete ui;
}
