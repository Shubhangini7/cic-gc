#ifndef LOGOUTWINDOW_H
#define LOGOUTWINDOW_H

#include <QMainWindow>

namespace Ui {
class LogOutWindow;
}

class LogOutWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit LogOutWindow(QWidget *parent = 0);
    ~LogOutWindow();

private:
    Ui::LogOutWindow *ui;
};

#endif // LOGOUTWINDOW_H
